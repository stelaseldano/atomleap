This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Install and run the project

`git clone https://stelaseldano@bitbucket.org/stelaseldano/atomleap.git`

`yarn install`

`yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
