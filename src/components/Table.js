import React from 'react';
import '../App.css';

const Table = props => {
	React.useEffect(() => {}, []);

	return (
		<div>
			<table>
				<tr>
					<th>Id</th>
					<th>Category</th>
					<th>Funding Amount</th>
					<th>Location</th>
					<th>Date</th>
				</tr>
				{props.data.map(row => (
					<tr>
						<td>{row.id}</td>
						<td>{row.category}</td>
						<td>{row.fundingAmount}</td>
						<td>{row.location}</td>
						<td>{row.announcedDate}</td>
					</tr>
				))}
			</table>
		</div>
	);
};

export default Table;
