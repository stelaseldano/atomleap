import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import HC_more from 'highcharts/highcharts-more';
import '../App.css';
HC_more(Highcharts);

const ChartContainer = props => {
	const [series, setSeries] = React.useState([]);
	const [loaded, setLoaded] = React.useState(false);

	React.useEffect(() => {
		const series = transformData(props.data);

		setSeries(series);
		setLoaded(true);
	}, []);

	const transformData = data => {
		let transformed = {};
		let series = [];

		for (var cat in data) {
			if (data.hasOwnProperty(cat)) {
				transformed = {
					name: cat,
					y: data[cat]['totalFunding'],
					x: data[cat]['rounds'].length,
					z: data[cat]['totalFunding']
				};
			}

			series.push(transformed);
		}

		return series;
	};

	const options = {
		chart: {
			type: 'bubble',
			plotBorderWidth: 1,
			zoomType: 'xy'
		},

		legend: {
			enabled: false
		},

		title: {
			text: 'Chart'
		},

		xAxis: {
			gridLineWidth: 1,
			title: {
				text: 'Rounds'
			},
			labels: {
				format: '{value}'
			}
		},

		yAxis: {
			title: {
				text: 'Total Funding'
			},
			labels: {
				format: '{value}'
			},
			maxPadding: 0.2
		},

		plotOptions: {
			series: {
				dataLabels: {
					enabled: true,
					format: '{point.name}'
				},
				point: {
					events: {
						click: function(ev) {
							props.updateSelectedCat(ev.point.name);
						}
					}
				}
			}
		},

		series: [
			{
				allowPointSelect: true,
				data: series
			}
		]
	};

	return (
		<div>
			{loaded && <HighchartsReact highcharts={Highcharts} options={options} />}
		</div>
	);
};

export default ChartContainer;
