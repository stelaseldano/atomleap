import React from 'react';
import './App.css';

import ChartContainer from './components/ChartContainer';
import Table from './components/Table';

function App() {
	const [data, setData] = React.useState();
	const [selectedCat, setSelectedCat] = React.useState();
	const [rowData, setRowData] = React.useState();
	const [selectedRowData, setSelectedRowData] = React.useState();
	const [loaded, setLoaded] = React.useState(false);

	React.useEffect(() => {
		fetch('http://demo0377384.mockable.io/funding-test')
			.then(response => {
				return response.json();
			})
			.then(data => {
				const dataObj = data.reduce((res, curr) => {
					if (curr.category in res) {
						res[curr.category]['rounds'].push(curr);
						res[curr.category]['totalFunding'] += curr.fundingAmount;

						return res;
					}

					res[curr.category] = {};
					res[curr.category]['rounds'] = [];
					res[curr.category]['rounds'].push(curr);
					res[curr.category]['totalFunding'] = curr.fundingAmount;
					res[curr.category]['category'] = curr.category;
					res[curr.category]['location'] = curr.location;

					return res;
				}, {});

				setData(dataObj);
				setRowData(data);
				setSelectedRowData(data);
				setLoaded(true);
			})
			.catch(err => {
				console.error(err);
			});
	}, []);

	React.useEffect(() => {
		if (rowData) {
			let newRowData = rowData.filter(data => data.category === selectedCat);

			setSelectedRowData(newRowData);
		}
	}, [selectedCat]);

	return (
		<div className="App">
			<header className="App-header">
				<h1>Funding by Industry Analytics</h1>
			</header>

			<main>
				{loaded && (
					<div>
						<ChartContainer
							data={data}
							updateSelectedCat={cat => {
								setSelectedCat(cat);
							}}
						/>
						<Table data={selectedRowData} selectedCat={selectedCat}></Table>
					</div>
				)}
			</main>
		</div>
	);
}

export default App;
